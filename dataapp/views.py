from django.shortcuts import render


def main(request):
    return render(request, 'dataapp/index.html')
