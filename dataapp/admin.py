from django.contrib import admin
from dataapp.models import *

# Register your models here.
admin.site.register(Symptom)
admin.site.register(Case)
admin.site.register(CustomUser)
admin.site.register(CaseSymptoms)
admin.site.register(ScreeningData)
admin.site.register(ScreeningDataQuestionAnswers)
