from django.urls import path

from dataapp.api.views import *
from coviddata.rest_api import *

urlpatterns = [
    path('api_v1/get_token', get_token),
    path('api_v1/signup', signup),
    path('api_v1/symptom', SymptomList.as_view()),
    path('api_v1/case_symptom', CaseSymptomList.as_view()),
    path('api_v1/case', CaseList.as_view()),
    path('api_v1/profile', get_profile),
    path('api_v1/get_statistics', get_statistics),
    path('api_v1/get_world_chart', get_chart_data_world),
    path('api_v1/post_screening', screening_submit),
    path('api_v1/get_screening', get_screening_results),

]