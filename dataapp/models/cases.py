from django.db import models


def ct_upload_to(instance, filename):
    return f"case/ct/{instance.id}.{filename}"


def x_ray_upload_to(instance, filename):
    return f"case/xray/{instance.id}.{filename}"


def cough_sound_upload_to(instance, filename):
    return f"case/cough/{instance.id}.{filename}"


def other_upload_to(instance, filename):
    return f"case/other/{instance.id}.{filename}"


class Symptom(models.Model):
    name = models.CharField(max_length=500)

    def __str__(self):
        return str(self.name)


class Case(models.Model):
    added_by = models.ForeignKey(to='dataapp.CustomUser', on_delete=models.SET_NULL, null=True)
    age = models.IntegerField(default=30)
    is_covid = models.BooleanField(default=False)
    lymphocytopenia = models.BooleanField(default=False)
    crp = models.BooleanField(default=False)
    ldh = models.BooleanField(default=False)
    esr = models.BooleanField(default=False)
    d_dimer = models.BooleanField(default=False)
    albumin = models.BooleanField(default=False)
    hemoglobin = models.BooleanField(default=False)

    ct_scan = models.ImageField(upload_to=ct_upload_to, null=True, blank=True)
    x_ray = models.ImageField(upload_to=x_ray_upload_to, null=True, blank=True)
    other_file = models.FileField(upload_to=other_upload_to, null=True, blank=True)


class CaseSymptoms(models.Model):
    case = models.ForeignKey(to=Case, on_delete=models.CASCADE)
    symptom = models.ForeignKey(to=Symptom, on_delete=models.CASCADE)

    def __str__(self):
        return str(str(self.case.id) + ' ' + self.symptom.name)





