from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    class Position(models.TextChoices):
        DOCTOR = 'DOCTOR'
        NURSE = 'NURSE'
        NONE = 'NONE'
    position = models.CharField(max_length=6, choices=Position.choices, default=Position.NONE)
    longitude = models.FloatField(null=True)
    latitude = models.FloatField(null=True)


