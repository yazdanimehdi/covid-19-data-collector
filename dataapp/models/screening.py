from django.db import models


class ScreeningDataQuestionAnswers(models.Model):
    screening_data = models.ForeignKey(to='dataapp.ScreeningData', on_delete=models.CASCADE)
    question_number = models.IntegerField()
    question_answer_code = models.IntegerField()


class ScreeningData(models.Model):
    user = models.ForeignKey(to='dataapp.CustomUser', on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now_add=True, auto_created=True)


