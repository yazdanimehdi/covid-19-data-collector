from rest_framework import serializers
from dataapp.models import CaseSymptoms, Case, Symptom, CustomUser
from rest_framework.authtoken.models import Token


class UserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(write_only=True)

    class Meta:
        model = CustomUser
        fields = '__all__'

    def get_tokens(self, user):
        token = Token.objects.create(user=user)

        return token.key

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        token = self.get_tokens(user)
        return token


class SymptomSerializer(serializers.ModelSerializer):

    class Meta:
        model = Symptom
        fields = '__all__'


class CaseSerializer(serializers.ModelSerializer):
    symptoms = serializers.SerializerMethodField()

    def get_symptoms(self, model):
        return [CaseSymptomSerializer(x).data for x in CaseSymptoms.objects.filter(case=model)]

    class Meta:
        model = Case
        fields = '__all__'


class CaseSymptomSerializer(serializers.ModelSerializer):
    symptom_name = serializers.SerializerMethodField()

    def get_symptom_name(self, model):
        return model.symptom.name

    class Meta:
        model = CaseSymptoms
        fields = '__all__'

