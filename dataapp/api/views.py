from django.contrib.auth import authenticate
from rest_framework import generics, status
from rest_framework.authentication import TokenAuthentication, SessionAuthentication
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from dataapp.models import ScreeningDataQuestionAnswers, ScreeningData
from .serializer import *
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from datetime import datetime

DEFAULT_PAGE = 1


class CustomPagination(PageNumberPagination):
    page = DEFAULT_PAGE
    page_size = 10
    page_size_query_param = 'page_size'

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'total': self.page.paginator.count,
            'page': int(self.request.GET.get('page', DEFAULT_PAGE)),
            'page_size': int(self.request.GET.get('page_size', self.page_size)),
            'results': data
        })


class CaseList(generics.ListCreateAPIView):
    queryset = Case.objects.all()
    serializer_class = CaseSerializer
    pagination_class = CustomPagination


@authentication_classes((TokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
class SymptomList(generics.ListCreateAPIView):
    queryset = Symptom.objects.all()
    serializer_class = SymptomSerializer


@authentication_classes((TokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
class CaseSymptomList(generics.ListCreateAPIView):
    queryset = CaseSymptoms.objects.all()
    serializer_class = CaseSymptomSerializer


@api_view(['POST'])
def signup(request):
    username = request.data['username']
    try:
        user = CustomUser.objects.get(username=username)
        return Response(data="Username Exists", status=status.HTTP_200_OK)
    except CustomUser.DoesNotExist:
        password = request.data['password']
        position = request.data['position']
        longitude = request.data['longitude']
        latitude = request.data['latitude']
        user = CustomUser()
        user.set_password(password)
        user.username = username
        user.position = position
        user.latitude = latitude
        user.longitude = longitude
        user.save()
        token = Token.objects.get_or_create(user=user)
        return Response(data=token[0].key, status=status.HTTP_200_OK)


@api_view(['POST'])
def get_token(request):
    username = request.data['username']
    password = request.data['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        token = Token.objects.get_or_create(user=user)
        return Response(data=token[0].key, status=status.HTTP_200_OK)
    else:
        return Response(data=False, status=status.HTTP_200_OK)


@api_view(('GET',))
@authentication_classes((TokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def get_profile(request):
    return Response(data={'username': request.user.username, 'id': request.user.id},
                    status=status.HTTP_200_OK)


@api_view(('POST',))
@authentication_classes((TokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def screening_submit(request):
    screening_data = ScreeningData()
    screening_data.user = request.user
    screening_data.save()

    q1 = ScreeningDataQuestionAnswers()
    q1.screening_data = screening_data
    q1.question_number = 1
    q1.question_answer_code = request.data['q1']
    q1.save()

    q2 = ScreeningDataQuestionAnswers()
    q2.screening_data = screening_data
    q2.question_number = 2
    q2.question_answer_code = request.data['q2']
    q2.save()

    q5 = ScreeningDataQuestionAnswers()
    q5.screening_data = screening_data
    q5.question_number = 5
    q5.question_answer_code = request.data['q5']
    q5.save()

    q8 = ScreeningDataQuestionAnswers()
    q8.screening_data = screening_data
    q8.question_number = 8
    q8.question_answer_code = request.data['q8']
    q8.save()

    for answer in request.data['q3']:
        q3 = ScreeningDataQuestionAnswers()
        q3.screening_data = screening_data
        q3.question_number = 3
        q3.question_answer_code = answer
        q3.save()

    for answer in request.data['q4']:
        q4 = ScreeningDataQuestionAnswers()
        q4.screening_data = screening_data
        q4.question_number = 4
        q4.question_answer_code = answer
        q4.save()

    for answer in request.data['q6']:
        q6 = ScreeningDataQuestionAnswers()
        q6.screening_data = screening_data
        q6.question_number = 6
        q6.question_answer_code = answer
        q6.save()

    for answer in request.data['q7']:
        q7 = ScreeningDataQuestionAnswers()
        q7.screening_data = screening_data
        q7.question_number = 7
        q7.question_answer_code = answer
        q7.save()

    return Response(data=True, status=status.HTTP_200_OK)


@api_view(('GET',))
@authentication_classes((TokenAuthentication, SessionAuthentication))
@permission_classes((IsAuthenticated,))
def get_screening_results(request):
    screening_inst = ScreeningData.objects.filter(user=request.user)
    screening_all = []
    for item in screening_inst:
        data = ScreeningDataQuestionAnswers.objects.filter(screening_data=item)
        q1 = 0
        q2 = 0
        q5 = 0
        q8 = 0
        q3 = []
        q4 = []
        q6 = []
        q7 = []
        for question_answer in data:
            if question_answer.question_number == 1:
                q1 = question_answer.question_answer_code
            if question_answer.question_number == 2:
                q2 = question_answer.question_answer_code
            if question_answer.question_number == 5:
                q5 = question_answer.question_answer_code
            if question_answer.question_number == 8:
                q8 = question_answer.question_answer_code
            if question_answer.question_number == 3:
                q3.append(question_answer.question_answer_code)
            if question_answer.question_number == 4:
                q4.append(question_answer.question_answer_code)
            if question_answer.question_number == 6:
                q6.append(question_answer.question_answer_code)
            if question_answer.question_number == 7:
                q7.append(question_answer.question_answer_code)
        screening_all.append({
            'dateTime': item.date_time,
            'answers': {'q1': q1, 'q2': q2, 'q3': q3, 'q4': q4, 'q5': q5, 'q6': q6, 'q7': q7,
                        'q8': q8}})

    return Response(data=screening_all, status=status.HTTP_200_OK)
