from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import  status
from coviddata.models import Country, CovidRecord
from django.db.models import Sum


@api_view(('GET',))
def get_statistics(request):
    total_dict = {'totalConfirmed': 0, 'totalDeaths': 0, 'totalRecovered': 0}
    country_list = []
    for country in Country.objects.all():
        data = CovidRecord.objects.filter(country=country).latest()
        country_list.append({'name': country.name,
                             'totalConfirmed': data.total_cases,
                             'totalDeaths': data.total_deaths,
                             'totalRecovered': data.total_recovered})
        total_dict['totalConfirmed'] += data.total_cases
        total_dict['totalDeaths'] += data.total_deaths
        total_dict['totalRecovered'] += data.total_recovered
    return Response(data={'total': total_dict, 'country': country_list},
                    status=status.HTTP_200_OK)


@api_view(('GET',))
def get_chart_data_world(request):

    total_case = [(x['update_date'], x['confirmed']) for x in CovidRecord.objects.values('update_date').annotate(confirmed=Sum('total_cases'))]
    fatal_case = [(x['update_date'], x['fatal']) for x in CovidRecord.objects.values('update_date').annotate(fatal=Sum('total_deaths'))]

    return Response(data={'confirmed': total_case, 'fatal': fatal_case},
                    status=status.HTTP_200_OK)
