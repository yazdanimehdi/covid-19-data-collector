from django.db import models
import datetime

class Country(models.Model):
    name = models.TextField()
    lat = models.FloatField()
    long = models.FloatField()


class CovidRecord(models.Model):
    update_date = models.DateField(default=datetime.date.today)
    country = models.ForeignKey(to='coviddata.Country', on_delete=models.CASCADE)
    total_cases = models.IntegerField(default=0)
    total_deaths = models.IntegerField(default=0)
    total_recovered = models.IntegerField(default=9)

    class Meta:
        get_latest_by = ['update_date']

