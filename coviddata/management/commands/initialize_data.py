import requests
from django.core.management.base import BaseCommand
from datetime import datetime
from coviddata.models import Country, CovidRecord

HISTORY_DATA_URL = 'https://corona.lmao.ninja/v2/historical'


class Command(BaseCommand):

    def handle(self, *args, **options):
        data = requests.get(HISTORY_DATA_URL)
        country_data = {}
        if data.status_code == 200:
            data_dict = data.json()
            for c in data_dict:
                try:
                    c_name = c['country']
                    if c_name == 'taiwan*':
                        c_name = 'Taiwan'
                    if c_name == 'bosnia':
                        c_name = 'Bosnia and Herzegovina'

                    country = Country.objects.get(name__iexact=c_name)
                    if not country.name in country_data:
                        country_data[country.name] = {'cases': c['timeline']['cases'],
                                                      'deaths': c['timeline']['deaths']}
                    else:
                        for d in country_data[country.name]['cases']:
                            country_data[country.name]['cases'][d] += c['timeline']['cases'][d]
                            country_data[country.name]['deaths'][d] += c['timeline']['deaths'][d]
                except:
                    print(c['country'])
                    continue

            for c_name in country_data:
                c = country_data[c_name]
                for k in c['cases']:
                    record = CovidRecord()
                    record.country = Country.objects.get(name=c_name)
                    record.update_date = datetime.strptime(k, '%m/%d/%y').date()
                    record.total_cases = c['cases'][k]
                    record.total_deaths = c['deaths'][k]
                    record.save()

