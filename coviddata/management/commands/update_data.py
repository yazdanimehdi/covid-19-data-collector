from django.core.management.base import BaseCommand
import requests
from coviddata.models import Country, CovidRecord
from datetime import datetime
from django.utils.timezone import make_aware


COUNTRY_DATA_URL = 'https://corona.lmao.ninja/countries?sort=country'


class Command(BaseCommand):

    def handle(self, *args, **options):
        data = requests.get(COUNTRY_DATA_URL)
        if data.status_code == 200:
            data_dict = data.json()
            for country_data in data_dict:
                country, created = Country.objects.get_or_create(name=country_data['country'], defaults={
                    'lat': country_data['countryInfo']['lat'],
                    'long': country_data['countryInfo']['long'],
                })
                if created:
                    country.save()
                # Check new record
                record_time = datetime.fromtimestamp(int(country_data['updated']) / 1000)
                try:
                    last_record = CovidRecord.objects.get(country=country, update_date=record_time.date())
                except CovidRecord.DoesNotExist:
                    last_record = CovidRecord()
                    last_record.country = country

                last_record.total_cases = country_data['cases']
                last_record.total_deaths = country_data['deaths']
                last_record.total_recovered = country_data['recovered']
                last_record.update_time = record_time
                last_record.save()
