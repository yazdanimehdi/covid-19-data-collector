from django.apps import AppConfig


class CoviddataConfig(AppConfig):
    name = 'coviddata'
